See [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to giro3d.

The following people have contributed to giro3d.

* [IGN](http://www.ign.fr)
  * [Gérald Choqueux](https://github.com/gchoqueux)
  * [Alexandre Devaux](https://github.com/nosy-b)
  * [Victor Coindet](https://github.com/VictorCo)
  * [Quoc Dinh Nguyen](https://github.com/qdnguyen)
  * [Grégoire Maillet](https://github.com/gmaillet)
  * [Guillaume Liégard](https://github.com/gliegard)
  * [Adrien Berthet](https://github.com/zarov)
  * [Mirela Konini](https://github.com/Mkonini)
  * Pierre Munier
  * Mathieu Bredif

* [Oslandia](http://www.oslandia.com)
  * [Vincent Picavet](https://github.com/vpicavet)
  * [Jérémy Gaillard](https://github.com/Jeremy-Gaillard)
  * [Pierre-Éric Pelloux-Prayer](https://github.com/peppsac)
  * [Augustin Trancart](https://github.com/autra)
  * [Sébastien Guimmara](https://github.com/sguimmara)
  * [Antoine Facchini](https://gitlab.com/antoinefacchini)
  * [François Thierry](https://github.com/Francois-Thierry)
  * [Benoît Blanc](https://github.com/benoitblanc)
  * [Thomas Muguet](https://github.com/tmuguet)
  * [Sylvain Beorchia](https://github.com/sylvainbeo)
  * [Éric Lemoine](https://github.com/elemoine)
  * [Benoît Ducarouge](https://github.com/Ducarouge)
  * [Frédéric Bonifas](https://github.com/fredericbonifas)
  * [Vincent Mora](https://github.com/vmora)

* [AtolCD](http://www.atolcd.com)
  * [Thomas Broyer](https://github.com/tbroyer)

* [LIRIS](https://liris.cnrs.fr/)
  * [Nicolas Saul](https://github.com/NikoSaul)
  * [Emmanuel Schmück](https://github.com/EmmanuelSchmuck/)
  * [Marie Lamure](https://github.com/mlamure)
  * [Vincent Jaillot](https://github.com/jailln)

The following organizations supported giro3d :
* IGN ( http://www.ign.fr )
* Oslandia ( http://www.oslandia.com )
* AtolCD ( http://www.atolcd.com )
